require 'test_helper'

class HomePageBlocksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @home_page_block = home_page_blocks(:one)
  end

  test "should get index" do
    get home_page_blocks_url
    assert_response :success
  end

  test "should get new" do
    get new_home_page_block_url
    assert_response :success
  end

  test "should create home_page_block" do
    assert_difference('HomePageBlock.count') do
      post home_page_blocks_url, params: { home_page_block: { body: @home_page_block.body, title: @home_page_block.title } }
    end

    assert_redirected_to home_page_block_url(HomePageBlock.last)
  end

  test "should show home_page_block" do
    get home_page_block_url(@home_page_block)
    assert_response :success
  end

  test "should get edit" do
    get edit_home_page_block_url(@home_page_block)
    assert_response :success
  end

  test "should update home_page_block" do
    patch home_page_block_url(@home_page_block), params: { home_page_block: { body: @home_page_block.body, title: @home_page_block.title } }
    assert_redirected_to home_page_block_url(@home_page_block)
  end

  test "should destroy home_page_block" do
    assert_difference('HomePageBlock.count', -1) do
      delete home_page_block_url(@home_page_block)
    end

    assert_redirected_to home_page_blocks_url
  end
end

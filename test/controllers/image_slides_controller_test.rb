require 'test_helper'

class ImageSlidesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @image_slide = image_slides(:one)
  end

  test "should get index" do
    get image_slides_url
    assert_response :success
  end

  test "should get new" do
    get new_image_slide_url
    assert_response :success
  end

  test "should create image_slide" do
    assert_difference('ImageSlide.count') do
      post image_slides_url, params: { image_slide: { body: @image_slide.body, title: @image_slide.title } }
    end

    assert_redirected_to image_slide_url(ImageSlide.last)
  end

  test "should show image_slide" do
    get image_slide_url(@image_slide)
    assert_response :success
  end

  test "should get edit" do
    get edit_image_slide_url(@image_slide)
    assert_response :success
  end

  test "should update image_slide" do
    patch image_slide_url(@image_slide), params: { image_slide: { body: @image_slide.body, title: @image_slide.title } }
    assert_redirected_to image_slide_url(@image_slide)
  end

  test "should destroy image_slide" do
    assert_difference('ImageSlide.count', -1) do
      delete image_slide_url(@image_slide)
    end

    assert_redirected_to image_slides_url
  end
end

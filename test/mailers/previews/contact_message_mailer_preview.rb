# Preview all emails at http://localhost:3000/rails/mailers/contact_message_mailer
class ContactMessageMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/contact_message_mailer/contact_me
  def contact_me
    ContactMessageMailer.contact_me
  end

end

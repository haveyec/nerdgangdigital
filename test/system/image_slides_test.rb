require "application_system_test_case"

class ImageSlidesTest < ApplicationSystemTestCase
  setup do
    @image_slide = image_slides(:one)
  end

  test "visiting the index" do
    visit image_slides_url
    assert_selector "h1", text: "Image Slides"
  end

  test "creating a Image slide" do
    visit image_slides_url
    click_on "New Image Slide"

    fill_in "Body", with: @image_slide.body
    fill_in "Title", with: @image_slide.title
    click_on "Create Image slide"

    assert_text "Image slide was successfully created"
    click_on "Back"
  end

  test "updating a Image slide" do
    visit image_slides_url
    click_on "Edit", match: :first

    fill_in "Body", with: @image_slide.body
    fill_in "Title", with: @image_slide.title
    click_on "Update Image slide"

    assert_text "Image slide was successfully updated"
    click_on "Back"
  end

  test "destroying a Image slide" do
    visit image_slides_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Image slide was successfully destroyed"
  end
end

require "application_system_test_case"

class HomePageBlocksTest < ApplicationSystemTestCase
  setup do
    @home_page_block = home_page_blocks(:one)
  end

  test "visiting the index" do
    visit home_page_blocks_url
    assert_selector "h1", text: "Home Page Blocks"
  end

  test "creating a Home page block" do
    visit home_page_blocks_url
    click_on "New Home Page Block"

    fill_in "Body", with: @home_page_block.body
    fill_in "Title", with: @home_page_block.title
    click_on "Create Home page block"

    assert_text "Home page block was successfully created"
    click_on "Back"
  end

  test "updating a Home page block" do
    visit home_page_blocks_url
    click_on "Edit", match: :first

    fill_in "Body", with: @home_page_block.body
    fill_in "Title", with: @home_page_block.title
    click_on "Update Home page block"

    assert_text "Home page block was successfully updated"
    click_on "Back"
  end

  test "destroying a Home page block" do
    visit home_page_blocks_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Home page block was successfully destroyed"
  end
end

class CreateHomePageBlocks < ActiveRecord::Migration[5.2]
  def change
    create_table :home_page_blocks do |t|
      t.string :title
      t.text :body

      t.timestamps
    end
  end
end

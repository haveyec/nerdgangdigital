class CreateImageSlides < ActiveRecord::Migration[5.2]
  def change
    create_table :image_slides do |t|
      t.string :title
      t.text :body

      t.timestamps
    end
  end
end

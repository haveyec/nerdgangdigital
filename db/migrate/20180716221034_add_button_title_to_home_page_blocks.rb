class AddButtonTitleToHomePageBlocks < ActiveRecord::Migration[5.2]
  def change
    add_column :home_page_blocks, :button_title, :string
  end
end

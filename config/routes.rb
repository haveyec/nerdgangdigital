Rails.application.routes.draw do
  resources :contact_messages
  resources :social_media
  resources :home_page_blocks
  resources :image_slides
  resources :pages
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :clients
  resources :homes
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

root 'homes#index'
get 'contact-us', to: 'contact_messages#new', as: 'new_message'

end

module ApplicationHelper

	def brand_name
	@name_of_brand ||= "Nerd Gang"	
	end

	def site_title
		@site_name ||= "::..Nerd Gang..:: When it comes to code we do our own stunts"
	end
	
	def footer
		"© #{brand_name} 2018." + ' ' + 'All Rights Reserved.'
	end

	def designed_by
		@sly_talk ||= "Another Digital hit by: "
		@sly_talk + "#{brand_name}"
	end

	def my_slider
		@imageSlide = ImageSlide.all
	end
end

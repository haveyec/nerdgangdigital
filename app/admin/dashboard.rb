ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    div class: "blank_slate_container", id: "dashboard_default_message" do
      span class: "blank_slate" do
        span I18n.t("active_admin.dashboard_welcome.welcome")
        small I18n.t("active_admin.dashboard_welcome.call_to_action")
      end
    end

    # Here is an example of a simple dashboard with columns and panels.
    #
    # columns do
    #   column do
    #     panel "Recent Posts" do
    #       ul do
    #         Post.recent(5).map do |post|
    #           li link_to(post.title, admin_post_path(post))
    #         end
    #       end
    #     end
    #   end

    #   column do
    #     panel "Info" do
    #       para "Welcome to ActiveAdmin."
    #     end
    #   end
    # end
    
    tabs do
        tab :pages do
            table_for pages do
                
                column :title do
                    Page.all.map do |page|
                    li link_to(page.title, admin_page_path(page))   
                    end
                        end

                        column :content do
                    Page.all.map do |page|
                    li link_to(page.body, admin_page_path(page))   
                    end
                        end
            end
        end

        tab :slider_slides do
            table_for pages do
                column :title do
                    ImageSlide.all.map do |image_slide|
                    li link_to(image_slide.title, admin_page_path(image_slide))   
                    end
                        end

                        column :body do
                    ImageSlide.all.map do |image_slide|
                    li link_to(image_slide.body, admin_page_path(image_slide))   
                    end
                        end


            end
        end

        tab :homepage_blocks do
            table_for pages do
                column :title, span: 2 do
                    HomePageBlock.all.map do |image_slide|
                    li link_to(image_slide.title, admin_page_path(image_slide))   
                    end
                        end

                        column :body do
                    HomePageBlock.all.map do |image_slide|
                    li link_to(image_slide.body, admin_page_path(image_slide))   
                    end
                        end

                        end
                        
                    end
        end





  end # content
end

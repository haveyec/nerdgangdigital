ActiveAdmin.register HomePageBlock do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
 permit_params :title, :body, :button_title, :image
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

form do |f|
	f.inputs "Details" do
		f.input :title, :label => "Title"
		f.input :body, :label => "Body"
		f.input :button_title, :label => "Button Display Text"
	f.input :image, as: :file
	f.actions
	end
	
end

end

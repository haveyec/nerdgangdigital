ActiveAdmin.register SocialMedia do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
 permit_params :title, :link, :image
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

form do |f|
	f.inputs "Details" do
		f.input :title, :label => "Title for social media"
		f.input :link, :label => "Link to social media account"
	f.input :image, as: :file
	f.actions
	end
	
end

end

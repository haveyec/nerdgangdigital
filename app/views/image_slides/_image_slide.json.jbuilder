json.extract! image_slide, :id, :title, :body, :image, :created_at, :updated_at
json.url image_slide_url(image_slide, format: :json)

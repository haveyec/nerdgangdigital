json.extract! home_page_block, :id, :title, :body, :created_at, :updated_at
json.url home_page_block_url(home_page_block, format: :json)

class ImageSlidesController < InheritedResources::Base

  private

    def image_slide_params
      params.require(:image_slide).permit(:title, :body,:image)
    end
end


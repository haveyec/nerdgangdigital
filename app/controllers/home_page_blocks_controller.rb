class HomePageBlocksController < InheritedResources::Base

  private

  def json_for_image
  	self.all.map {|x| url_for(x.image) }.to_json
  end

    def home_page_block_params
      params.require(:home_page_block).permit(:title, :body, :image)
    end
end


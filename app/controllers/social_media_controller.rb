class SocialMediaController < InheritedResources::Base

  private

    def social_medium_params
      params.require(:social_medium).permit(:title, :link, :image)
    end
end


class ContactMessagesController < InheritedResources::Base

  private

    def contact_message_params
      params.require(:contact_message).permit(:name, :email, :message)
    end
end

